const heads = process.env.HEADS;
const tails = process.env.TAILS;

module.exports.handler = function(event, context, done) {
  const flip = Boolean(Math.round(Math.random()));
  done(null, {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin': '*'
    },
    body: flip ? heads : tails
  });
}
