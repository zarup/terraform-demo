# Terraform Lunch 'n Learn Demo

There are a bunch of files here with names like "stepX.tf.nope". The idea is that the represent incremental development of a terraform project. The ".nope" file extension keeps Terraform from trying to apply them all.

To start, rename step2.tf.nope to step2.tf and then run these:

  terraform init
  terraform apply

When asked for an "env_name" use your name, like "colby".

To progress through the examples, rename the current n.tf file back to n.tf.nope and rename the next one to .tf. Each time you'll want to run those two commands.

## Clean Up

When you're done, run `terraform destroy` to remove your project from AWS.
